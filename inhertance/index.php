<?php
    include_once ("vendor\autoload.php");

    use learning_php\Bangla\video as BanglaVideo;
    use learning_php\English\video;

    $bangla = new BanglaVideo();
    $bangla->movie();

    $english = new video();
    $english->movie();
?>